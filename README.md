## Project info

This training project was made to analyse general performance of basic 

### 


### Helpful commands
MYSQL setup:
To pull mysql docker image:
docker pull mysql/mysql-server:latest

To start new container:
docker run -d --name mysql-web-shooter --restart unless-stopped --env="MYSQL_ROOT_PASSWORD=password" -p 3306:3306 mysql

Connect to mysql container instance via cmd line:
docker exec -it mysql-web-shooter bash
then enter mysql:
mysql -u root -p

database required scripts
CREATE DATABASE web_shooter_base;
CREATE USER 'shooter'@'localhost' IDENTIFIED WITH mysql_native_password BY 'shooterpass';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, REFERENCES, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON web_shooter_base.* TO 'shooter'@'localhost';

in windows docker desktop use proxy/gateway IP instead of localhost:
CREATE USER 'shooter'@'172.17.0.1' IDENTIFIED WITH mysql_native_password BY 'shooterpass';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, INDEX, REFERENCES, DROP, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES ON web_shooter_base.* TO 'shooter'@'172.17.0.1';

CREATE TABLE IF NOT EXISTS car (
    id bigint NOT NULL AUTO_INCREMENT PRIMARY KEY,
    creation_time timestamp,
    last_update_time timestamp,
    brand varchar(50) unique
)  DEFAULT CHARSET=UTF8;

INSERT INTO car VALUES (1, '2020-01-01', '2020-01-01', 'syrena');

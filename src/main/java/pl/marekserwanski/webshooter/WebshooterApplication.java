package pl.marekserwanski.webshooter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebshooterApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshooterApplication.class, args);
	}

}

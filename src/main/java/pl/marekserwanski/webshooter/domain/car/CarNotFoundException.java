package pl.marekserwanski.webshooter.domain.car;

class CarNotFoundException extends RuntimeException {

    CarNotFoundException() {
        super ("Car not found");
    }
}

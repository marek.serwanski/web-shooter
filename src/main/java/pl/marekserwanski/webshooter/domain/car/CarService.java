package pl.marekserwanski.webshooter.domain.car;

import pl.marekserwanski.webshooter.utils.MeasurableDto;

import java.sql.Timestamp;
import java.util.List;

import static java.time.Instant.now;
import static java.util.stream.Collectors.toList;
import static org.springframework.data.domain.Pageable.ofSize;

public class CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public CarDto getFirstCar() {
        return carRepository.findById(1L).map(carMapper::map).orElseThrow(CarNotFoundException::new);
    }

    public List<MeasurableDto> getCarsList(int count) {
        return carRepository.findAll(ofSize(count)).stream().map(carMapper::map).collect(toList());
    }

    public void updateFirstCarTimestamp() {
        carRepository.findById(1L).ifPresentOrElse(
            car -> {
                car.setLastUpdateTime(Timestamp.from(now()));
                carRepository.save(car);
            }, () -> {
                throw new CarNotFoundException();
            });
    }

    public void addNewCar(String brand) {
        carRepository.findByBrand(brand).ifPresent(car -> {throw new CarAlreadyExistException(car.getBrand());});
        carRepository.save(new Car(brand));
    }

    public void deleteCar(String brand) {
        carRepository.findByBrand(brand).ifPresent(carRepository::delete);
    }


}

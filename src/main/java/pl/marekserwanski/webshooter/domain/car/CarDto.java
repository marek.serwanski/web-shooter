package pl.marekserwanski.webshooter.domain.car;

import pl.marekserwanski.webshooter.utils.MeasurableDto;

public class CarDto implements MeasurableDto {

    private final String brand;

    public CarDto(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }
}

package pl.marekserwanski.webshooter.domain.car;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

interface CarRepository extends CrudRepository<Car, Long> {

    Optional<Car> findByBrand(String brand);

    List<Car> findAll(Pageable page);
}

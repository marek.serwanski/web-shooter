package pl.marekserwanski.webshooter.domain.car;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CarConfig {

    @Bean
    CarMapper carMapper() {
        return new CarMapper();
    }

    @Bean
    CarService carService(CarRepository carRepository, CarMapper carMapper) {
        return new CarService(carRepository, carMapper);
    }
}

package pl.marekserwanski.webshooter.domain.user;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static org.apache.tomcat.util.codec.binary.Base64.encodeBase64;

public class UserDaoAuthenticator {

    //Hardcoded values. Remember - this application is only for performance measuring purposes
    static final String defaultUsername = "user";
    static final String defaultPass = "pass";

    private final DaoAuthenticationProvider authenticationProvider;

    public UserDaoAuthenticator(DaoAuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    SecurityContext authenticateDefaultUser() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(defaultUsername, defaultPass);
        Authentication auth = authenticationProvider.authenticate(token);

        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(auth);
        return sc;
    }

    String createDefaultUserAuthHeader() {
        String baseHeader = defaultUsername + ":" + defaultPass;
        return "Basic " + new String(encodeBase64(baseHeader.getBytes(ISO_8859_1)));
    }
}

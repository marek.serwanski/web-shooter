package pl.marekserwanski.webshooter.domain.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

@Configuration
public class UserConfig {

    @Bean
    public UserService userService(UserRepository userRepository) {
        return new UserService(userRepository);
    }

    @Bean
    public UserDaoAuthenticator userDaoAuthenticator(DaoAuthenticationProvider authenticationProvider) {
        return new UserDaoAuthenticator(authenticationProvider);
    }

}

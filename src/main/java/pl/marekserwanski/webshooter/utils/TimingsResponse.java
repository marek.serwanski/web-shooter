package pl.marekserwanski.webshooter.utils;

public class TimingsResponse {

    protected final Long frontendStartTime;
    protected final Long backendTotal;

    protected TimingsResponse(Long frontendStartTime, Long backendTotal) {
        this.frontendStartTime = frontendStartTime;
        this.backendTotal = backendTotal;
    }

    public static TimingsResponse buildSimpleResponse(Long frontendStartTime, Long backendTotal) {
        return new TimingsResponse(frontendStartTime, backendTotal);
    }

    public Long getBackendTotal() {
        return backendTotal;
    }

    public Long getFrontendStartTime() {
        return frontendStartTime;
    }
}

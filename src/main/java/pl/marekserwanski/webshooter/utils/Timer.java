package pl.marekserwanski.webshooter.utils;

import org.slf4j.Logger;
import pl.marekserwanski.webshooter.target.AuthType;

import java.util.Date;

import static org.slf4j.LoggerFactory.getLogger;

/** Use it to count backend time between request receiving and returning response */
public class Timer {

    private static final Logger log = getLogger(Timer.class);
    private final Date startTime;

    private Timer() {
        this.startTime = new Date();
    }

    public static Timer start() {
        return start(AuthType.open);
    }

    public static Timer start(AuthType authType) {
        Timer timer = new Timer();
        log.info("Init request server time counting at {}. After auth type: {}", timer.getProccessTimeInMS(), authType);
        return timer;
    }

    public Long getProccessTimeInMS() {
        long currentTime = new Date().getTime();
        long result = currentTime - startTime.getTime();
        log.info("Finish server time counting at {}. Total result: {}", currentTime, result);
        return result;
    }
}

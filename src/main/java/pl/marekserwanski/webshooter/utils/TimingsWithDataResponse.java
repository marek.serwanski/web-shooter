package pl.marekserwanski.webshooter.utils;

import java.util.List;

public class TimingsWithDataResponse extends TimingsResponse {

    private final List<MeasurableDto> measurableDtos;

    private TimingsWithDataResponse(Long frontendStartTime, Long backendTotal, List<MeasurableDto> measurableDtos) {
        super(frontendStartTime, backendTotal);
        this.measurableDtos = measurableDtos;
    }

    public static TimingsWithDataResponse buildResponseWithDtos(Long frontendStartTime, Long backendTotal, List<MeasurableDto> measurableDtos) {
        return new TimingsWithDataResponse(frontendStartTime, backendTotal, measurableDtos);
    }

    public List<MeasurableDto> getMeasurableDtos() {
        return measurableDtos;
    }
}

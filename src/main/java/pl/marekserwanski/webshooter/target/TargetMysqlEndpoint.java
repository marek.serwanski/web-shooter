package pl.marekserwanski.webshooter.target;

import org.springframework.web.bind.annotation.*;
import pl.marekserwanski.webshooter.domain.car.CarDto;
import pl.marekserwanski.webshooter.utils.*;

import java.util.List;

import static java.util.Arrays.asList;
import static pl.marekserwanski.webshooter.utils.Timer.start;
import static pl.marekserwanski.webshooter.utils.TimingsResponse.buildSimpleResponse;
import static pl.marekserwanski.webshooter.utils.TimingsWithDataResponse.buildResponseWithDtos;

@RestController
@RequestMapping("/db")
public class TargetMysqlEndpoint {

    private final TargetMysqlActions actions;

    TargetMysqlEndpoint(TargetMysqlActions targetMysqlActions) {
        this.actions = targetMysqlActions;
    }

    @GetMapping("simple/{authType}")
    public TimingsWithDataResponse simpleGetRecordFromDB(@RequestParam Long frontendStartTime, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        CarDto car = actions.simpleGetRecordFromDB();
        return buildResponseWithDtos(frontendStartTime, timer.getProccessTimeInMS(), asList(car));
    }

    @PostMapping("list")
    public TimingsWithDataResponse getListFromDB(@RequestBody TimingsRequest timingsRequest) {
        Timer timer = start();
        List<MeasurableDto> cars = actions.getListFromDB(timingsRequest.getActionsCount());
        return buildResponseWithDtos(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS(), cars);
    }

    @PostMapping("simple/{authType}")
    public TimingsResponse makeSimpleRecordUpdate(@RequestBody TimingsRequest timingsRequest, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        actions.makeSimpleRecordUpdate();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }

    @PostMapping("crud/{authType}")
    public TimingsResponse makeSimpleCRUDactions(@RequestBody TimingsRequest timingsRequest, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        actions.makeSimpleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }

    @PostMapping("multiplecrud/{authType}")
    public TimingsResponse makeComplexCRUDactions(@RequestBody TimingsRequest timingsRequest, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        actions.makeMultipleCRUDactions();
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }


}

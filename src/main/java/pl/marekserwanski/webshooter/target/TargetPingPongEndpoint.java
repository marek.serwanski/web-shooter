package pl.marekserwanski.webshooter.target;

import org.springframework.web.bind.annotation.*;
import pl.marekserwanski.webshooter.utils.Timer;
import pl.marekserwanski.webshooter.utils.TimingsRequest;
import pl.marekserwanski.webshooter.utils.TimingsResponse;

import static pl.marekserwanski.webshooter.utils.Timer.start;
import static pl.marekserwanski.webshooter.utils.TimingsResponse.buildSimpleResponse;

@RestController
@RequestMapping("/pingpong/{authType}")
public class TargetPingPongEndpoint {

    @GetMapping
    public TimingsResponse makeGetPingPong(Long frontendStartTime, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        return buildSimpleResponse(frontendStartTime, timer.getProccessTimeInMS());
    }

    @PostMapping
    TimingsResponse makePostPingPong(TimingsRequest timingsRequest, @PathVariable("authType") AuthType authType) {
        Timer timer = start(authType);
        return buildSimpleResponse(timingsRequest.getFrontendStartTime(), timer.getProccessTimeInMS());
    }
}

package pl.marekserwanski.webshooter.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.marekserwanski.webshooter.domain.user.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] OPEN_URI = {"/**/open", "/login"};

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        //authorization
        http.authorizeRequests()
                .antMatchers(OPEN_URI).permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();

        //disable CSRF
        http.csrf().disable();

        //enable CORS
        http.cors();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider(UserService userService) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userService);
        authProvider.setPasswordEncoder(new BCryptPasswordEncoder());
        return authProvider;
    }
}

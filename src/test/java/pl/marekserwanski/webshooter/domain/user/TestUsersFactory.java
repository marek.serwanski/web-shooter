package pl.marekserwanski.webshooter.domain.user;

import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultPass;
import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultUsername;

class TestUsersFactory {

    static User defaultUser() {
        User user = new User();
        user.setUsername(defaultUsername);
        user.setPassword(defaultPass);
        return user;
    }
}

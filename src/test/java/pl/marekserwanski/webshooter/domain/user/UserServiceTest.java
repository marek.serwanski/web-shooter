package pl.marekserwanski.webshooter.domain.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.webshooter.domain.user.TestUsersFactory.defaultUser;
import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultPass;
import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultUsername;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService service;

    @Test
    public void shouldLoadByUserName() {
        when(userRepository.findByUsername(defaultUsername)).thenReturn(Optional.of(defaultUser()));

        UserDetails result = service.loadUserByUsername(defaultUsername);

        assertThat(result.getUsername()).isEqualTo(defaultUsername);
        assertThat(result.getPassword()).isEqualTo(defaultPass);
    }

}

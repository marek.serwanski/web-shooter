package pl.marekserwanski.webshooter.domain.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultPass;
import static pl.marekserwanski.webshooter.domain.user.UserDaoAuthenticator.defaultUsername;

@ExtendWith(MockitoExtension.class)
public class UserDaoAuthenticatorTest {

    @Mock
    DaoAuthenticationProvider authenticationProvider;
    @InjectMocks
    UserDaoAuthenticator authenticator;

    @Test
    public void shouldAuthenticateUser() {
        Authentication authentication = new UsernamePasswordAuthenticationToken(defaultUsername, defaultPass);
        when(authenticationProvider.authenticate(any(Authentication.class))).thenReturn(authentication);

        SecurityContext securityContext = authenticator.authenticateDefaultUser();

        Authentication result = securityContext.getAuthentication();
        assertThat(result.getPrincipal()).isEqualTo(defaultUsername);
        assertThat(result.getCredentials()).isEqualTo(defaultPass);
    }
}

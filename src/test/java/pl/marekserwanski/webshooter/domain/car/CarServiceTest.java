package pl.marekserwanski.webshooter.domain.car;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.marekserwanski.webshooter.utils.MeasurableDto;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.marekserwanski.webshooter.domain.car.TestCarsFactory.defaultCar;
import static pl.marekserwanski.webshooter.domain.car.TestCarsFactory.dummyBrand;

@ExtendWith(MockitoExtension.class)
public class CarServiceTest {

    @Mock
    CarRepository carRepository;
    @Spy
    CarMapper carMapper = new CarMapper();

    @InjectMocks
    CarService carService;

    @Test
    public void shouldGetFirstCar() {
        Car car = defaultCar();
        when(carRepository.findById(1L)).thenReturn(Optional.of(car));

        CarDto result = carService.getFirstCar();

        assertThat(result).isNotNull();
        assertThat(result.getBrand()).isEqualTo(car.getBrand());
    }

    @Test
    public void shouldNotFoundCar() {
        assertThrows(CarNotFoundException.class, () -> carService.getFirstCar());
    }

    @Test
    public void shouldUdateFirstCarTimestamp() {
        Car car = defaultCar();
        when(carRepository.findById(1L)).thenReturn(Optional.of(car));

        carService.updateFirstCarTimestamp();

        assertThat(car.getLastUpdateTime()).isNotNull();
    }

    @Test
    public void shouldAddNewCar() {
        carService.addNewCar(dummyBrand);
        verify(carRepository).save(any(Car.class));
    }

    @Test
    public void shouldNotAddNewCar() {
        when(carRepository.findByBrand(dummyBrand)).thenReturn(Optional.of(defaultCar()));
        assertThrows(CarAlreadyExistException.class, () -> carService.addNewCar(dummyBrand));
    }

    @Test
    public void shouldGetListOfCars() {
        when(carRepository.findAll(any())).thenReturn(asList(defaultCar()));
        List<MeasurableDto> result = carService.getCarsList(50);

        assertThat(result).isNotNull();
        assertThat(((CarDto)result.get(0)).getBrand()).isEqualTo(dummyBrand);
    }
}

package pl.marekserwanski.webshooter.domain.car;

class TestCarsFactory {
    static final String dummyBrand = "syrena";

    static Car defaultCar() {
        return new Car(dummyBrand);
    }
}
